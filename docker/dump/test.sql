CREATE TABLE IF NOT EXISTS client
(
    id          integer      not null
    constraint client_pkey
    primary key,
    firstname   varchar(32)  not null,
    lastname    varchar(32)  not null,
    email       varchar(254) not null,
    phonenumber varchar(17)  not null,
    createdat   timestamp(0) not null,
    updatedat   timestamp(0) default NULL::timestamp without time zone
);

CREATE TABLE IF NOT EXISTS application
(
    id        integer       not null
        constraint application_pkey
            primary key,
    clientid  integer       not null,
    term      integer       not null,
    amount    numeric(8, 2) not null,
    currency  varchar(3)    not null,
    createdat timestamp(0)  not null,
    updatedat timestamp(0) default NULL::timestamp without time zone
);

CREATE SEQUENCE client_id_seq INCREMENT BY 1 MINVALUE 1 START 1;
CREATE SEQUENCE application_id_seq INCREMENT BY 1 MINVALUE 1 START 1;