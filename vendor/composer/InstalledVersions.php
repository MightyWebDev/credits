<?php











namespace Composer;

use Composer\Autoload\ClassLoader;
use Composer\Semver\VersionParser;








class InstalledVersions
{
private static $installed = array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'api-platform/core' => 
    array (
      'pretty_version' => 'v2.6.4',
      'version' => '2.6.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'be971e5d0257a8c01a824ee01cfc217ba04c3200',
    ),
    'behat/gherkin' => 
    array (
      'pretty_version' => 'v4.8.0',
      'version' => '4.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2391482cd003dfdc36b679b27e9f5326bd656acd',
    ),
    'codeception/codeception' => 
    array (
      'pretty_version' => '4.1.21',
      'version' => '4.1.21.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c25f20d842a7e3fa0a8e6abf0828f102c914d419',
    ),
    'codeception/lib-asserts' => 
    array (
      'pretty_version' => '1.13.2',
      'version' => '1.13.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '184231d5eab66bc69afd6b9429344d80c67a33b6',
    ),
    'codeception/lib-innerbrowser' => 
    array (
      'pretty_version' => '1.5.0',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4b0d89b37fe454e060a610a85280a87ab4f534f1',
    ),
    'codeception/module-asserts' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '59374f2fef0cabb9e8ddb53277e85cdca74328de',
    ),
    'codeception/module-cli' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1f841ad4a1d43e5d9e60a43c4cc9e5af8008024f',
    ),
    'codeception/module-db' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8c8076cd05d4db95798acd7dba2a56578210982c',
    ),
    'codeception/module-doctrine2' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a39cd444fac840c907d33d33bca83e57dd0e04d1',
    ),
    'codeception/module-laravel' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f32a941a06e57bb06d2e485da11b191f38bec659',
    ),
    'codeception/module-laravel5' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d8a82f78a6e8c26f49af65d9001fa311785d54b',
    ),
    'codeception/module-phpbrowser' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '770a6be4160a5c0c08d100dd51bff35f6056bbf1',
    ),
    'codeception/module-rest' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '293a0103d5257b7c884ef276147a9a06914e878f',
    ),
    'codeception/module-symfony' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '3ee80157fac45d883e7bf209a3b9cddd3fc13d5c',
    ),
    'codeception/module-webdriver' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ebbe729c630415e8caf6b0087e457906f0c6c0c6',
    ),
    'codeception/phpunit-wrapper' => 
    array (
      'pretty_version' => '9.0.6',
      'version' => '9.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b0c06abb3181eedca690170f7ed0fd26a70bfacc',
    ),
    'codeception/stub' => 
    array (
      'pretty_version' => '3.7.0',
      'version' => '3.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '468dd5fe659f131fc997f5196aad87512f9b1304',
    ),
    'composer/package-versions-deprecated' => 
    array (
      'pretty_version' => '1.11.99.2',
      'version' => '1.11.99.2',
      'aliases' => 
      array (
      ),
      'reference' => 'c6522afe5540d5fc46675043d3ed5a45a740b27c',
    ),
    'doctrine/annotations' => 
    array (
      'pretty_version' => '1.13.1',
      'version' => '1.13.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e6e7b7d5b45a2f2abc5460cc6396480b2b1d321f',
    ),
    'doctrine/cache' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c9622c6820d3ede1e2315a6a377ea1076e421d88',
    ),
    'doctrine/collections' => 
    array (
      'pretty_version' => '1.6.7',
      'version' => '1.6.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '55f8b799269a1a472457bd1a41b4f379d4cfba4a',
    ),
    'doctrine/common' => 
    array (
      'pretty_version' => '3.1.2',
      'version' => '3.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a036d90c303f3163b5be8b8fde9b6755b2be4a3a',
    ),
    'doctrine/dbal' => 
    array (
      'pretty_version' => '2.13.x-dev',
      'version' => '2.13.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '2c193548ca5ba7b1d66c9590f079fb2240fd0302',
    ),
    'doctrine/deprecations' => 
    array (
      'pretty_version' => 'v0.5.3',
      'version' => '0.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9504165960a1f83cc1480e2be1dd0a0478561314',
    ),
    'doctrine/doctrine-bundle' => 
    array (
      'pretty_version' => '2.4.2',
      'version' => '2.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '4202ce675d29e70a8b9ee763bec021b6f44caccb',
    ),
    'doctrine/doctrine-migrations-bundle' => 
    array (
      'pretty_version' => '3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '91f0a5e2356029575f3038432cc188b12f9d5da5',
    ),
    'doctrine/event-manager' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '41370af6a30faa9dc0368c4a6814d596e81aba7f',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9cf661f4eb38f7c881cac67c75ea9b00bf97b210',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd56bf6102915de5702778fe20f2de3b2fe570b5b',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
    ),
    'doctrine/migrations' => 
    array (
      'pretty_version' => '3.1.2',
      'version' => '3.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '1c2780df6b58998f411e64973cfa464ba0a06e00',
    ),
    'doctrine/orm' => 
    array (
      'pretty_version' => '2.9.2',
      'version' => '2.9.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '75b4b88c5b7cebc24ed7251a20c2a5aa027300e1',
    ),
    'doctrine/persistence' => 
    array (
      'pretty_version' => '2.2.1',
      'version' => '2.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd138f3ab5f761055cab1054070377cfd3222e368',
    ),
    'doctrine/sql-formatter' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '56070bebac6e77230ed7d306ad13528e60732871',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c81f18a3efb941d8c4d2e025f6183b5c6d697307',
    ),
    'facebook/webdriver' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'fig/link-util' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '5d7b8d04ed3393b4b59968ca1e906fb7186d81e8',
    ),
    'flow/jsonpath' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'friendsofphp/proxy-manager-lts' => 
    array (
      'pretty_version' => 'v1.0.5',
      'version' => '1.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '006aa5d32f887a4db4353b13b5b5095613e0611f',
    ),
    'friendsofsymfony/rest-bundle' => 
    array (
      'pretty_version' => '3.0.5',
      'version' => '3.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '8779ceebf715d1c60bd10286fce9d32ed03c484a',
    ),
    'giggsey/libphonenumber-for-php' => 
    array (
      'pretty_version' => '8.12.24',
      'version' => '8.12.24.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bbefdd7ff0bae7bf5a62fc9e8c939fb40dd6d638',
    ),
    'giggsey/locale' => 
    array (
      'pretty_version' => '1.9',
      'version' => '1.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b07f1eace8072ccc61445ad8fbd493ff9d783043',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '7.3.0',
      'version' => '7.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7008573787b430c1c1f650e3722d9bba59967628',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => '1.4.1',
      'version' => '1.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8e7d04f1f6450fef59366c399cfad4b9383aa30d',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.8.2',
      'version' => '1.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dc960a912984efb74d0a90222870c72c87f10c91',
    ),
    'justinrainbow/json-schema' => 
    array (
      'pretty_version' => '5.2.10',
      'version' => '5.2.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '2ba9c8c862ecd5510ed16c6340aa9f6eadb4f31b',
    ),
    'laminas/laminas-code' => 
    array (
      'pretty_version' => '4.3.0',
      'version' => '4.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1beb4447f9efd26041eba7eff50614e798c353fd',
    ),
    'laminas/laminas-eventmanager' => 
    array (
      'pretty_version' => '3.3.1',
      'version' => '3.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '966c859b67867b179fde1eff0cd38df51472ce4a',
    ),
    'laminas/laminas-zendframework-bridge' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6cccbddfcfc742eb02158d6137ca5687d92cee32',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1cb1cde8e8dd0f70cc0fe51354a59acad9302084',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.10.2',
      'version' => '1.10.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '776f831124e9c62e1a2c601ecc52e776d8bb7220',
      'replaced' => 
      array (
        0 => '1.10.2',
      ),
    ),
    'nelmio/cors-bundle' => 
    array (
      'pretty_version' => '2.1.1',
      'version' => '2.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '0b964b665016dfb61dd0fd2bb8c24afb1ae45a93',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.10.5',
      'version' => '4.10.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '4432ba399e47c66624bc73c8c0f811e5c109576f',
    ),
    'ocramius/package-versions' => 
    array (
      'replaced' => 
      array (
        0 => '1.11.99',
      ),
    ),
    'ocramius/proxy-manager' => 
    array (
      'replaced' => 
      array (
        0 => '^2.1',
      ),
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '85265efd3af7ba3ca4b2a2c34dbfc5788dd29133',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bae7c545bef187884426f042434e561ab1ddb182',
    ),
    'php-http/async-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '*',
      ),
    ),
    'php-http/client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '*',
      ),
    ),
    'php-webdriver/webdriver' => 
    array (
      'pretty_version' => '1.11.1',
      'version' => '1.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'da16e39968f8dd5cfb7d07eef91dc2b731c69880',
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d01c49d4ed62f25aa84a747ad35d5a16924662b',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '5.2.2',
      'version' => '5.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '069a785b2141f5bcf49f3e353548dc1cce6df556',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6a467b8989322d92aa1c8bf2bebcc6e5c2ba55c0',
    ),
    'phpspec/prophecy' => 
    array (
      'pretty_version' => '1.13.0',
      'version' => '1.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'be1996ed8adc35c3fd795488a653f4b518be70ea',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '9.2.6',
      'version' => '9.2.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6293e1b30a2354e8428e004689671b83871edde',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '3.0.5',
      'version' => '3.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aa4be8575f26070b100fccb67faabb28f21f66f8',
    ),
    'phpunit/php-invoker' => 
    array (
      'pretty_version' => '3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5a10147d0aaf65b58940a0b72f71c9ac0423cc67',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '2.0.4',
      'version' => '2.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '5da5f67fc95621df9ff4c4e5a84d6a8a2acf7c28',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '5.0.3',
      'version' => '5.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5a63ce20ed1b5bf577850e2c4e87f4aa902afbd2',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '9.5.5',
      'version' => '9.5.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '89ff45ea9d70e35522fb6654a2ebc221158de276',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/event-dispatcher' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dbefd12671e8a14ec7f180cab83036ed26714bb0',
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
    ),
    'psr/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/link' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eea8e8662d5cd3ae4517c9b864493f59fca95562',
    ),
    'psr/link-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0',
        1 => '1.0',
      ),
    ),
    'psr/simple-cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'sebastian/cli-parser' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '442e7c7e687e42adc03470c7b668bc4b2402c0b2',
    ),
    'sebastian/code-unit' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '1fc9f64c0927627ef78ba436c9b17d967e68e120',
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ac91f01ccec49fb77bdc6fd1e548bc70f7faa3e5',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '4.0.6',
      'version' => '4.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '55f4261989e546dc112258c7a75935a81a7ce382',
    ),
    'sebastian/complexity' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '739b35e53379900cc9ac327b2147867b8b6efd88',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '4.0.4',
      'version' => '4.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '3461e3fccc7cfdfc2720be910d3bd73c69be590d',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '5.1.3',
      'version' => '5.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '388b6ced16caa751030f6a69e588299fa09200ac',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '4.0.3',
      'version' => '4.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd89cc98761b8cb5a1a235a6b703ae50d34080e65',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '5.0.2',
      'version' => '5.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a90ccbddffa067b51f574dea6eb25d5680839455',
    ),
    'sebastian/lines-of-code' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c1c2e997aa3146983ed888ad08b15470a2e22ecc',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '4.0.4',
      'version' => '4.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '5c9eeac41b290a3712d88851518825ad78f45c71',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => '2.0.4',
      'version' => '2.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b4f479ebdbf63ac605d183ece17d8d7fe49c15c7',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '4.0.4',
      'version' => '4.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd9d8cf3c5804de4341c283ed787f099f5506172',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f4443cb3a1d92ce809899753bc0d5d5a8dd19a8',
    ),
    'sebastian/type' => 
    array (
      'pretty_version' => '2.3.2',
      'version' => '2.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '0d1c587401514d17e8f9258a27e23527cb1b06c1',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6c1022351a901512170118436c764e473f6de8c',
    ),
    'sensio/framework-extra-bundle' => 
    array (
      'pretty_version' => 'v6.1.5',
      'version' => '6.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '62c5909f49cf74dccdf50a294511cc24be2f969c',
    ),
    'softcreatr/jsonpath' => 
    array (
      'pretty_version' => '0.7.5',
      'version' => '0.7.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '008569bf80aa3584834f7890781576bc7b65afa7',
    ),
    'symfony/asset' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '465943866ccfe37082ebfea7a3ac8b4e0179d6de',
    ),
    'symfony/browser-kit' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '4ec25538fdba87442ad043231f4d208cac6eda77',
    ),
    'symfony/cache' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'b602a626efbb17711aca9748e5a0341a98dec599',
    ),
    'symfony/cache-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c0446463729b89dd4fa62e9aeecc80287323615d',
    ),
    'symfony/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'symfony/config' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'db7f82ee863ec4ebaed87fb2b036a76510259da9',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'bcbfa4b4868e63f875380d8cee373a6a2a0153ea',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '5d5f97809015102116208b976eb2edb44b689560',
    ),
    'symfony/debug-bundle' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'ea0d4732b3f807f90c62301f1dda54f8b2d34b6f',
    ),
    'symfony/dependency-injection' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '2348d4ac592ca74e49d51bf7ccf56bc22b3108b3',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f38c8804a9e97d23e0c8d63341088cd8a22d627',
    ),
    'symfony/doctrine-bridge' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '7fb05ad7936d3750876c7594f3439a5d4cad7749',
    ),
    'symfony/dom-crawler' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '194246b6c6481985963b641d1540f757913782c0',
    ),
    'symfony/dotenv' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '4ababad35dbb739b521614d3094d7e5e0d1d06e4',
    ),
    'symfony/error-handler' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'f5ae1a4298b754594c66535bbce0f0d6e06c008d',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'aecf90ffee8f1d2a2c169ec51cd1177027fc3ace',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '69fee1ad2332a7cbab3aca13591953da9cdb7a11',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0',
      ),
    ),
    'symfony/expression-language' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '324a7c08fc19fbda9c96c13962dfb2e17f54a531',
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '348116319d7fb7d1faa781d26a48922428013eb2',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'ccccb9d48ca42757dd12f2ca4bf857a4e217d90d',
    ),
    'symfony/flex' => 
    array (
      'pretty_version' => 'v1.13.3',
      'version' => '1.13.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '2597d0dda8042c43eed44a9cd07236b897e427d7',
    ),
    'symfony/form' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'a962ac5105f73951fc32d71fdfb37d10fd9f4e1e',
    ),
    'symfony/framework-bundle' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '414b55783a9d9e45fd2f0798c3ba11d33293a4b4',
    ),
    'symfony/http-client' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'abac7e02da270806868346746e9084cbc2e5f3ed',
    ),
    'symfony/http-client-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7e82f6084d7cae521a75ef2cb5c9457bbda785f4',
    ),
    'symfony/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.4',
      ),
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'd776c3bd60a7a529175b3c4b282f1fecdf6d7b9f',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '6486ca4f71214eb04d0009efb19aab44b488b6ef',
    ),
    'symfony/intl' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'bb7376e843910a2ee650431013463cc6a83f456c',
    ),
    'symfony/mailer' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '6dca77f3d0038edb17bcb5970f744a516578d586',
    ),
    'symfony/maker-bundle' => 
    array (
      'pretty_version' => 'v1.31.1',
      'version' => '1.31.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4f57a44cef0b4555043160b8bf223fcde8a7a59a',
    ),
    'symfony/mime' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '8210beb33ece7f0624d87b0f49dc70bdfcef2cfa',
    ),
    'symfony/monolog-bridge' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '9e336ff9a9ea7433179d4f961a32ef7a603d7324',
    ),
    'symfony/monolog-bundle' => 
    array (
      'pretty_version' => 'v3.7.0',
      'version' => '3.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4054b2e940a25195ae15f0a49ab0c51718922eb4',
    ),
    'symfony/notifier' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '8720dfe4b6d1c7fed545f1b2dc7072d7c7eabfc6',
    ),
    'symfony/options-resolver' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '162e886ca035869866d233a2bfef70cc28f9bbe5',
    ),
    'symfony/password-hasher' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '1e66f5423f44a175b32333c3197990bd2ab702cc',
    ),
    'symfony/phpunit-bridge' => 
    array (
      'pretty_version' => 'v5.3.0',
      'version' => '5.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '15cab721487b7bf43ad545a1e7d0095782e26f8c',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-iconv' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '24b72c6baa32c746a4d0840147c9715e42bb68ab',
    ),
    'symfony/polyfill-intl-icu' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4a80a521d6176870b6445cfb469c130f9cae1dda',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '65bd267525e82759e7d8c4e8ceea44f398838e65',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8590a5f561694770bdcd3f9b5c69dde6945028e8',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2df51500adbaebdc4c38dea4c89a2e131c45c8a1',
    ),
    'symfony/polyfill-php72' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fba8933c384d6476ab14fb7b8526e5287ca7e010',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eca0bf41ed421bed1b57c4958bab16aa86b757d0',
    ),
    'symfony/polyfill-php81' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e66119f3de95efc359483f810c4c3e6436279436',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '53e36cb1c160505cdaf1ef201501669c4c317191',
    ),
    'symfony/property-access' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'fd89ef387030f4d4126fd9cddcc18dbfed27a346',
    ),
    'symfony/property-info' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'a45839949b03e1437837000ce8319a9214974501',
    ),
    'symfony/proxy-manager-bridge' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'a87444e1c0ae52d732011733c2d0baf183d34e9e',
    ),
    'symfony/routing' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'd9b66a07fbbf0c7728f9654a26f3c89cfa15bcd6',
    ),
    'symfony/runtime' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '02c710b4448b35d3a12da0c315ee065100ac7778',
    ),
    'symfony/security-bundle' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '03cd95b5318b26d302fc5c703ebbc20fb27a1763',
    ),
    'symfony/security-core' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'ef371bb0646e8202df02f1c94afbd1d19c0b1820',
    ),
    'symfony/security-csrf' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '03010e75105759cf171f3429133e26801186ed85',
    ),
    'symfony/security-guard' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '240c71e09ce3f6acc2251495855353737aacff02',
    ),
    'symfony/security-http' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '0f66d617e96f0ed540459945a233302a2243da40',
    ),
    'symfony/serializer' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '915121cd03627c632f44e3642d7cf7ad407e23e3',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f040a30e04b57fbcc9c6cbcf4dbaa96bd318b9bb',
    ),
    'symfony/service-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'symfony/stopwatch' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '8b99a525d24c264990d07f8db3e709d11b2651e9',
    ),
    'symfony/string' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '07417941ec86fa429b4b108e1217a656816483d5',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '3710e2fd9d43cfe7bc88a4312ce95f6a5509bd49',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '95c812666f3e91db75385749fe219c5e494c7f95',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.3',
      ),
    ),
    'symfony/twig-bridge' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '218a1ada2f7d0ce96225878c5818f9069dcfdae0',
    ),
    'symfony/twig-bundle' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '2aebe44a60887244fb2b21a17b50045ed3fabd2b',
    ),
    'symfony/validator' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '7c214ec359bccf393dffe0fc2a34ad587a646176',
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '173cf39010292d1de114f3a086e50fd27dc9d0d6',
    ),
    'symfony/var-exporter' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '74b0c1b4116640dcd34240c2a4cd61c71fb24a4e',
    ),
    'symfony/web-link' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'f1e15f28812865dbf3ffe68e7feba6b23fc5f667',
    ),
    'symfony/web-profiler-bundle' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '06b5b3d6efe8f46176eaf9328cf1dc217072bf53',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '7cda21186dabba9fc064e5991cbc72f9851049c1',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '75a63c33a8577608444246075ea0af0d052e452a',
    ),
    'twig/extra-bundle' => 
    array (
      'pretty_version' => 'v3.3.1',
      'version' => '3.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e12a8ee63387abb83fb7e4c897663bfb94ac22b6',
    ),
    'twig/twig' => 
    array (
      'pretty_version' => 'v3.3.2',
      'version' => '3.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '21578f00e83d4a82ecfa3d50752b609f13de6790',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6964c76c7804814a842473e0c8fd15bab0f18e25',
    ),
    'willdurand/jsonp-callback-validator' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1a7d388bb521959e612ef50c5c7b1691b097e909',
    ),
    'willdurand/negotiation' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '04e14f38d4edfcc974114a07d2777d90c98f3d9c',
    ),
    'zendframework/zend-code' => 
    array (
      'replaced' => 
      array (
        0 => '4.3.0',
      ),
    ),
    'zendframework/zend-eventmanager' => 
    array (
      'replaced' => 
      array (
        0 => '^3.2.1',
      ),
    ),
  ),
);
private static $canGetVendors;
private static $installedByVendor = array();







public static function getInstalledPackages()
{
$packages = array();
foreach (self::getInstalled() as $installed) {
$packages[] = array_keys($installed['versions']);
}

if (1 === \count($packages)) {
return $packages[0];
}

return array_keys(array_flip(\call_user_func_array('array_merge', $packages)));
}









public static function isInstalled($packageName)
{
foreach (self::getInstalled() as $installed) {
if (isset($installed['versions'][$packageName])) {
return true;
}
}

return false;
}














public static function satisfies(VersionParser $parser, $packageName, $constraint)
{
$constraint = $parser->parseConstraints($constraint);
$provided = $parser->parseConstraints(self::getVersionRanges($packageName));

return $provided->matches($constraint);
}










public static function getVersionRanges($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

$ranges = array();
if (isset($installed['versions'][$packageName]['pretty_version'])) {
$ranges[] = $installed['versions'][$packageName]['pretty_version'];
}
if (array_key_exists('aliases', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['aliases']);
}
if (array_key_exists('replaced', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['replaced']);
}
if (array_key_exists('provided', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['provided']);
}

return implode(' || ', $ranges);
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getVersion($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['version'])) {
return null;
}

return $installed['versions'][$packageName]['version'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getPrettyVersion($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['pretty_version'])) {
return null;
}

return $installed['versions'][$packageName]['pretty_version'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getReference($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['reference'])) {
return null;
}

return $installed['versions'][$packageName]['reference'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getRootPackage()
{
$installed = self::getInstalled();

return $installed[0]['root'];
}








public static function getRawData()
{
@trigger_error('getRawData only returns the first dataset loaded, which may not be what you expect. Use getAllRawData() instead which returns all datasets for all autoloaders present in the process.', E_USER_DEPRECATED);

return self::$installed;
}







public static function getAllRawData()
{
return self::getInstalled();
}



















public static function reload($data)
{
self::$installed = $data;
self::$installedByVendor = array();
}





private static function getInstalled()
{
if (null === self::$canGetVendors) {
self::$canGetVendors = method_exists('Composer\Autoload\ClassLoader', 'getRegisteredLoaders');
}

$installed = array();

if (self::$canGetVendors) {
foreach (ClassLoader::getRegisteredLoaders() as $vendorDir => $loader) {
if (isset(self::$installedByVendor[$vendorDir])) {
$installed[] = self::$installedByVendor[$vendorDir];
} elseif (is_file($vendorDir.'/composer/installed.php')) {
$installed[] = self::$installedByVendor[$vendorDir] = require $vendorDir.'/composer/installed.php';
}
}
}

$installed[] = self::$installed;

return $installed;
}
}
