<?php

namespace ContainerZDxhydt;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolderd4ad6 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer09fbb = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesd1f0d = [
        
    ];

    public function getConnection()
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'getConnection', array(), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'getMetadataFactory', array(), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'getExpressionBuilder', array(), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'beginTransaction', array(), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'getCache', array(), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->getCache();
    }

    public function transactional($func)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'transactional', array('func' => $func), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->transactional($func);
    }

    public function commit()
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'commit', array(), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->commit();
    }

    public function rollback()
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'rollback', array(), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'getClassMetadata', array('className' => $className), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'createQuery', array('dql' => $dql), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'createNamedQuery', array('name' => $name), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'createQueryBuilder', array(), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'flush', array('entity' => $entity), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'clear', array('entityName' => $entityName), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->clear($entityName);
    }

    public function close()
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'close', array(), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->close();
    }

    public function persist($entity)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'persist', array('entity' => $entity), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'remove', array('entity' => $entity), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'refresh', array('entity' => $entity), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'detach', array('entity' => $entity), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'merge', array('entity' => $entity), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'getRepository', array('entityName' => $entityName), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'contains', array('entity' => $entity), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'getEventManager', array(), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'getConfiguration', array(), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'isOpen', array(), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'getUnitOfWork', array(), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'getProxyFactory', array(), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'initializeObject', array('obj' => $obj), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'getFilters', array(), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'isFiltersStateClean', array(), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'hasFilters', array(), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return $this->valueHolderd4ad6->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer09fbb = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolderd4ad6) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolderd4ad6 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolderd4ad6->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, '__get', ['name' => $name], $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        if (isset(self::$publicPropertiesd1f0d[$name])) {
            return $this->valueHolderd4ad6->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd4ad6;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderd4ad6;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd4ad6;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderd4ad6;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, '__isset', array('name' => $name), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd4ad6;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolderd4ad6;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, '__unset', array('name' => $name), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd4ad6;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolderd4ad6;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, '__clone', array(), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        $this->valueHolderd4ad6 = clone $this->valueHolderd4ad6;
    }

    public function __sleep()
    {
        $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, '__sleep', array(), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;

        return array('valueHolderd4ad6');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer09fbb = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer09fbb;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer09fbb && ($this->initializer09fbb->__invoke($valueHolderd4ad6, $this, 'initializeProxy', array(), $this->initializer09fbb) || 1) && $this->valueHolderd4ad6 = $valueHolderd4ad6;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolderd4ad6;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolderd4ad6;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
