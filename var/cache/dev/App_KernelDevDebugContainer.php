<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerZDxhydt\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerZDxhydt/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerZDxhydt.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerZDxhydt\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \ContainerZDxhydt\App_KernelDevDebugContainer([
    'container.build_hash' => 'ZDxhydt',
    'container.build_id' => 'c55616ba',
    'container.build_time' => 1623002174,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerZDxhydt');
