<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/api/v1/client' => [
            [['_route' => 'client_list', '_controller' => 'App\\Controller\\ClientController:listAction'], null, ['GET' => 0], null, false, false, null],
            [['_route' => 'client_create', '_controller' => 'App\\Controller\\ClientController:createAction'], null, ['POST' => 0], null, false, false, null],
        ],
        '/api/v1/application' => [
            [['_route' => 'application_list', '_controller' => 'App\\Controller\\ApplicationController:listAction'], null, ['GET' => 0], null, false, false, null],
            [['_route' => 'application_create', '_controller' => 'App\\Controller\\ApplicationController:createAction'], null, ['POST' => 0], null, false, false, null],
        ],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|wdt/([^/]++)(*:24)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:69)'
                            .'|router(*:82)'
                            .'|exception(?'
                                .'|(*:101)'
                                .'|\\.css(*:114)'
                            .')'
                        .')'
                        .'|(*:124)'
                    .')'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:159)'
                .')'
                .'|/api(?'
                    .'|(?:/(index)(?:\\.([^/]++))?)?(*:203)'
                    .'|/(?'
                        .'|docs(?:\\.([^/]++))?(*:234)'
                        .'|contexts/(.+)(?:\\.([^/]++))?(*:270)'
                        .'|v1/(?'
                            .'|client/(\\d+)(?'
                                .'|(*:299)'
                            .')'
                            .'|application/(\\d+)(?'
                                .'|(*:328)'
                            .')'
                        .')'
                    .')'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        24 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        69 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        82 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        101 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        114 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        124 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        159 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        203 => [[['_route' => 'api_entrypoint', '_controller' => 'api_platform.action.entrypoint', '_format' => '', '_api_respond' => 'true', 'index' => 'index'], ['index', '_format'], null, null, false, true, null]],
        234 => [[['_route' => 'api_doc', '_controller' => 'api_platform.action.documentation', '_format' => '', '_api_respond' => 'true'], ['_format'], null, null, false, true, null]],
        270 => [[['_route' => 'api_jsonld_context', '_controller' => 'api_platform.jsonld.action.context', '_format' => 'jsonld', '_api_respond' => 'true'], ['shortName', '_format'], null, null, false, true, null]],
        299 => [
            [['_route' => 'client_show', '_controller' => 'App\\Controller\\ClientController:showAction'], ['id'], ['GET' => 0], null, false, true, null],
            [['_route' => 'client_update', '_controller' => 'App\\Controller\\ClientController:updateAction'], ['id'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'client_delete', '_controller' => 'App\\Controller\\ClientController:deleteAction'], ['id'], ['DELETE' => 0], null, false, true, null],
        ],
        328 => [
            [['_route' => 'application_show', '_controller' => 'App\\Controller\\ApplicationController:showAction'], ['id'], ['GET' => 0], null, false, true, null],
            [['_route' => 'application_update', '_controller' => 'App\\Controller\\ApplicationController:updateAction'], ['id'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'application_delete', '_controller' => 'App\\Controller\\ApplicationController:deleteAction'], ['id'], ['DELETE' => 0], null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
