<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\Client;
use App\Validator\PhoneFormat;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class ClientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName', TextType::class, [
                'constraints' => [
                    new NotNull(),
                    new NotBlank(),
                    new Length([
                        'min' => 2,
                        'max' => 32
                    ])
                ]
            ])
            ->add('lastName', TextType::class, [
                'constraints' => [
                    new NotNull(),
                    new NotBlank(),
                    new Length([
                        'min' => 2,
                        'max' => 32
                    ])
                ]
            ])
            ->add('email', EmailType::class, [
                'constraints' => [
                    new NotNull(),
                    new NotBlank(),
                    new Email()
                ]
            ])
            ->add('phoneNumber', TextType::class, [
                'constraints' => [
                    new NotNull(),
                    new NotBlank(),
                    new PhoneFormat()
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Client::class
        ]);
    }


}