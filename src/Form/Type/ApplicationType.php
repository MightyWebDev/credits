<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\Application;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Currency;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class ApplicationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('clientId', IntegerType::class, [
                'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('term', TextType::class, [
                'constraints' => [
                    new NotNull(),
                    new GreaterThanOrEqual([
                        'value' => 10
                    ]),
                    new LessThanOrEqual([
                        'value' => 30
                    ])
                ]
            ])
            ->add('amount', NumberType::class, [
                'scale' => 2,
                'constraints' => [
                    new NotNull(),
                    new GreaterThanOrEqual([
                        'value' => 100.00
                    ]),
                    new LessThanOrEqual([
                        'value' => 5000.00
                    ])
                ]
            ])
            ->add('currency', TextType::class, [
                'constraints' => [
                    new NotNull(),
                    new NotBlank(),
                    new Length(3),
                    new Currency()
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Application::class
        ]);
    }


}