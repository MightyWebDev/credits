<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Client;
use App\Form\Type\ClientType;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ClientController extends AbstractApiController
{
    public function listAction(Request $request): Response
    {
        $pageSize = $request->get('pageSize', 10);
        $page = $request->get('page', 1);

        $query = $this->getDoctrine()
            ->getRepository(Client::class)
            ->createQueryBuilder('c')
            ->setMaxResults($pageSize)
            ->setFirstResult($pageSize * ($page - 1))
            ->getQuery();

        $paginator = new Paginator($query);

        return $this->respond(['data' => $paginator, 'total' => count($paginator)]);
    }

    public function showAction(Request $request): Response
    {
        $id = $request->get('id');

        if (!$id) {
            return $this->respond(['code' => Response::HTTP_BAD_REQUEST, 'message' => 'Id is not passed'], Response::HTTP_BAD_REQUEST);
        }

        $client = $this->getDoctrine()->getRepository(Client::class)->find($id);

        if (!$client) {
            return $this->respond(['code' => Response::HTTP_BAD_REQUEST, 'message' => 'Client not found'], Response::HTTP_BAD_REQUEST);
        }

        return $this->respond($client);
    }

    public function createAction(Request $request): Response
    {
        $form = $this->buildForm(ClientType::class);

        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }

        /** @var Client $client */
        $client = $form->getData();

        $this->getDoctrine()->getManager()->persist($client);
        $this->getDoctrine()->getManager()->flush();

        return $this->respond($client);
    }

    public function updateAction(Request $request): Response
    {
        $id = $request->get('id');

        if (!$id) {
            return $this->respond(['code' => Response::HTTP_BAD_REQUEST, 'message' => 'Id is not passed'], Response::HTTP_BAD_REQUEST);
        }

        $client = $this->getDoctrine()->getRepository(Client::class)->find($id);

        if (!$client) {
            return $this->respond(['code' => Response::HTTP_BAD_REQUEST, 'message' => 'Client not found'], Response::HTTP_BAD_REQUEST);
        }

        $form = $this->buildForm(ClientType::class, $client, [
            'method' => $request->getMethod()
        ]);

        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }

        /** @var Client $client */
        $client = $form->getData();

        $this->getDoctrine()->getManager()->persist($client);
        $this->getDoctrine()->getManager()->flush();

        return $this->respond($client);
    }

    public function deleteAction(Request $request): Response
    {
        $id = $request->get('id');

        if (!$id) {
            return $this->respond(['code' => Response::HTTP_BAD_REQUEST, 'message' => 'Id is not passed'], Response::HTTP_BAD_REQUEST);
        }

        $client = $this->getDoctrine()->getRepository(Client::class)->find($id);

        if (!$client) {
            return $this->respond(['code' => Response::HTTP_BAD_REQUEST, 'message' => 'Client not found'], Response::HTTP_BAD_REQUEST);
        }

        $this->getDoctrine()->getManager()->remove($client);
        $this->getDoctrine()->getManager()->flush();

        return $this->respond(null);
    }
}