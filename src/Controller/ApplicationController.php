<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Application;
use App\Entity\Client;
use App\Form\Type\ApplicationType;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApplicationController extends AbstractApiController
{
    public function listAction(Request $request): Response
    {
        $pageSize = $request->get('pageSize', 10);
        $page = $request->get('page', 1);

        $query = $this->getDoctrine()
            ->getRepository(Application::class)
            ->createQueryBuilder('a')
            ->setMaxResults($pageSize)
            ->setFirstResult($pageSize * ($page - 1))
            ->getQuery();

        $paginator = new Paginator($query);

        return $this->respond(['data' => $paginator, 'total' => count($paginator)]);
    }

    public function showAction(Request $request): Response
    {
        $id = $request->get('id');

        if (!$id) {
            return $this->respond(['code' => Response::HTTP_BAD_REQUEST, 'message' => 'Id is not passed'], Response::HTTP_BAD_REQUEST);
        }

        $application = $this->getDoctrine()->getRepository(Application::class)->find($id);

        if (!$application) {
            return $this->respond(['code' => Response::HTTP_BAD_REQUEST, 'message' => 'Application not found'], Response::HTTP_BAD_REQUEST);
        }

        return $this->respond($application);
    }

    public function createAction(Request $request): Response
    {
        $form = $this->buildForm(ApplicationType::class);

        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }

        /** @var Application $application */
        $application = $form->getData();

        $client = $this->getDoctrine()->getRepository(Client::class)->find($application->getClientId());
        if (!$client) {
            return $this->respond(['code' => Response::HTTP_BAD_REQUEST, 'message' => 'Client not found'], Response::HTTP_BAD_REQUEST);
        }

        $this->getDoctrine()->getManager()->persist($application);
        $this->getDoctrine()->getManager()->flush();

        return $this->respond($application);
    }

    public function updateAction(Request $request): Response
    {
        $id = $request->get('id');

        if (!$id) {
            return $this->respond(['code' => Response::HTTP_BAD_REQUEST, 'message' => 'Id is not passed'], Response::HTTP_BAD_REQUEST);
        }

        $application = $this->getDoctrine()->getRepository(Application::class)->find($id);

        if (!$application) {
            return $this->respond(['code' => Response::HTTP_BAD_REQUEST, 'message' => 'Application not found'], Response::HTTP_BAD_REQUEST);
        }

        $client = $this->getDoctrine()->getRepository(Client::class)->find($application->getClientId());
        if (!$client) {
            return $this->respond(['code' => Response::HTTP_BAD_REQUEST, 'message' => 'Client not found'], Response::HTTP_BAD_REQUEST);
        }

        $form = $this->buildForm(ApplicationType::class, $application, [
            'method' => $request->getMethod()
        ]);

        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }

        /** @var Application $application */
        $application = $form->getData();

        $this->getDoctrine()->getManager()->persist($application);
        $this->getDoctrine()->getManager()->flush();

        return $this->respond($application);
    }

    public function deleteAction(Request $request): Response
    {
        $id = $request->get('id');

        if (!$id) {
            return $this->respond(['code' => Response::HTTP_BAD_REQUEST, 'message' => 'Id is not passed'], Response::HTTP_BAD_REQUEST);
        }

        $application = $this->getDoctrine()->getRepository(Application::class)->find($id);

        if (!$application) {
            return $this->respond(['code' => Response::HTTP_BAD_REQUEST, 'message' => 'Application not found'], Response::HTTP_BAD_REQUEST);
        }

        $this->getDoctrine()->getManager()->remove($application);
        $this->getDoctrine()->getManager()->flush();

        return $this->respond(null);
    }
}