<?php

declare(strict_types=1);

namespace App\Validator;

use libphonenumber\NumberParseException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use libphonenumber\PhoneNumberUtil;


class PhoneFormatValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        $phoneNumberUtil = PhoneNumberUtil::getInstance();
        if (!$constraint instanceof PhoneFormat) {
            throw new UnexpectedTypeException($constraint, PhoneFormat::class);
        }

        if ($value === null || $value === '') {
            return;
        }

        if (!preg_match('/^[\d+]+$/', $value)) {
            return $this->context->buildViolation('Phone number has prohibited characters')
                ->addViolation();
        }

        try {
            $phoneValue = $phoneNumberUtil->parse($value, 'LV');
            if (!$phoneNumberUtil->isValidNumber($phoneValue)) {
                return $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ string }}', $value)
                    ->addViolation();
            }
        } catch (NumberParseException $e) {
            return $this->context->buildViolation($e->getMessage())
                ->addViolation();
        }
    }
}