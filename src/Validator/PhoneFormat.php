<?php

declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

class PhoneFormat extends Constraint
{
    public $message = 'String "{{ string }}" has a wrong phone format';

    public function validatedBy()
    {
        return static::class . 'Validator';
    }
}