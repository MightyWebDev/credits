# Client & Application REST API

Project is written on Symfony, uses PostgreSQL and Doctrine, is dockerized, tests are included. Swagger config file is added in the root of a project.   
## Requirements

* Composer
* Docker compose

## Installation

```bash
docker-compose up -d

docker-compose exec php ./bin/console doctrine:migrations:migrate -n
```

Or using Make

```bash
make up

make db
```


## Testing

```
docker-compose exec php vendor/bin/codecept run --steps
```

Or using Make

```
make test
```