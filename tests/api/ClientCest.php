<?php

use \Codeception\Util\HttpCode;

class ClientCest
{
    public function checkSuccessScenario(\ApiTester $I)
    {
        //Checking client creation
        $I->sendPost('/client', [
            'firstName' => 'John',
            'lastName' => 'Doe',
            'email' => 'john.doe@mail.com',
            'phoneNumber' => '+37121234567'
        ]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'firstName' => 'string',
            'lastName' => 'string',
            'email' => 'string:email',
            'phoneNumber' => 'string',
            'createdAt' => 'string:date',
            'updatedAt' => 'string:date'
        ]);
        list($id) = $I->grabDataFromResponseByJsonPath('$.id');

        //Checking client update
        $I->sendPut("/client/$id", [
            'firstName' => 'Elon',
            'lastName' => 'Musk',
            'email' => 'elon.musk@mail.com',
            'phoneNumber' => '+12122061204'
        ]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'firstName' => 'string',
            'lastName' => 'string',
            'email' => 'string:email',
            'phoneNumber' => 'string',
            'createdAt' => 'string:date',
            'updatedAt' => 'string:date'
        ]);

        //Checking get specific client
        $I->sendGet("/client/$id");
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'firstName' => 'string',
            'lastName' => 'string',
            'email' => 'string:email',
            'phoneNumber' => 'string',
            'createdAt' => 'string:date',
            'updatedAt' => 'string:date'
        ]);

        //Checking get client list
        $I->sendGet('/client');
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'data' => [
                [
                    'id' => 'integer',
                    'firstName' => 'string',
                    'lastName' => 'string',
                    'email' => 'string:email',
                    'phoneNumber' => 'string',
                    'createdAt' => 'string:date',
                    'updatedAt' => 'string:date'
                ]
            ],
            'total' => 'integer'
        ]);

        //Checking client deletion
        $I->sendDelete("/client/$id");
        $I->seeResponseCodeIs(HttpCode::OK);
    }


    public function checkFailScenario(\ApiTester $I)
    {
        //Empty data
        $I->sendPost('/client');
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        //Empty values
        $I->sendPost('/client', [
            'firstName' => '',
            'lastName' => '',
            'email' => '',
            'phoneNumber' => ''
        ]);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        //First name less than 2 characters
        $I->sendPost('/client', [
            'firstName' => '1',
            'lastName' => 'Doe',
            'email' => 'john.doe@mail.com',
            'phoneNumber' => '+37121234567'
        ]);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        //First name more than 32 characters
        $I->sendPost('/client', [
            'firstName' => '1aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa33',
            'lastName' => 'Doe',
            'email' => 'john.doe@mail.com',
            'phoneNumber' => '+37121234567'
        ]);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);



        //Last name less than 2 characters
        $I->sendPost('/client', [
            'firstName' => 'John',
            'lastName' => '1',
            'email' => 'john.doe@mail.com',
            'phoneNumber' => '+37121234567'
        ]);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        //Last name more than 32 characters
        $I->sendPost('/client', [
            'firstName' => 'John',
            'lastName' => '1aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa33',
            'email' => 'john.doe@mail.com',
            'phoneNumber' => '+37121234567'
        ]);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        //Invalid e-mail address
        $I->sendPost('/client', [
            'firstName' => 'John',
            'lastName' => 'Doe',
            'email' => 'email',
            'phoneNumber' => '+37121234567'
        ]);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        //Invalid phone number
        $I->sendPost('/client', [
            'firstName' => 'John',
            'lastName' => 'Doe',
            'email' => 'john.doe@mail.com',
            'phoneNumber' => '+3710'
        ]);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        //Wrong client id for get
        $I->sendGet('/client/200');
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        //Wrong client id for update
        $I->sendPut('/client/200');
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        //Wrong client id for delete
        $I->sendDelete('/client/200');
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);
    }
}