<?php

use \Codeception\Util\HttpCode;

class ApplicationCest
{
    public function checkSuccessScenario(\ApiTester $I)
    {
        //Creating client to get client id
        $I->sendPost('/client', [
            'firstName' => 'John',
            'lastName' => 'Doe',
            'email' => 'john.doe@mail.com',
            'phoneNumber' => '+37121234567'
        ]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'firstName' => 'string',
            'lastName' => 'string',
            'email' => 'string:email',
            'phoneNumber' => 'string',
            'createdAt' => 'string:date',
            'updatedAt' => 'string:date'
        ]);
        list($clientId) = $I->grabDataFromResponseByJsonPath('$.id');

        //Checking application creation
        $I->sendPost('/application', [
            'clientId' => $clientId,
            'term' => 30,
            'amount' => 2900.50,
            'currency' => 'EUR'
        ]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'clientId' => 'integer',
            'term' => 'integer',
            'amount' => 'float',
            'currency' => 'string',
            'createdAt' => 'string:date',
            'updatedAt' => 'string:date'
        ]);
        list($id) = $I->grabDataFromResponseByJsonPath('$.id');

        //Checking application update
        $I->sendPut("/application/$id", [
            'clientId' => $clientId,
            'term' => 15,
            'amount' => 2500.50,
            'currency' => 'EUR'
        ]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'clientId' => 'integer',
            'term' => 'integer',
            'amount' => 'float',
            'currency' => 'string',
            'createdAt' => 'string:date',
            'updatedAt' => 'string:date'
        ]);

        //Checking get specific application
        $I->sendGet("/application/$id");
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'clientId' => 'integer',
            'term' => 'integer',
            'amount' => 'float',
            'currency' => 'string',
            'createdAt' => 'string:date',
            'updatedAt' => 'string:date'
        ]);

        //Checking get application list
        $I->sendGet('/application');
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'data' => [
                [
                    'id' => 'integer',
                    'clientId' => 'integer',
                    'term' => 'integer',
                    'amount' => 'float',
                    'currency' => 'string',
                    'createdAt' => 'string:date',
                    'updatedAt' => 'string:date'
                ]
            ],
            'total' => 'integer'
        ]);

        //Checking application deletion
        $I->sendDelete("/application/$id");
        $I->seeResponseCodeIs(HttpCode::OK);
    }


    public function checkFailScenario(\ApiTester $I)
    {
        //Creating client to get client id
        $I->sendPost('/client', [
            'firstName' => 'John',
            'lastName' => 'Doe',
            'email' => 'john.doe@mail.com',
            'phoneNumber' => '+37121234567'
        ]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'firstName' => 'string',
            'lastName' => 'string',
            'email' => 'string:email',
            'phoneNumber' => 'string',
            'createdAt' => 'string:date',
            'updatedAt' => 'string:date'
        ]);
        list($clientId) = $I->grabDataFromResponseByJsonPath('$.id');

        //Empty data
        $I->sendPost('/application');
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        //Empty values
        $I->sendPost('/application', [
            'clientId' => '',
            'term' => '',
            'amount' => '',
            'currency' => ''
        ]);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        //Wrong client id
        $I->sendPost('/client', [
            'clientId' => 'aaaaa',
            'term' => 30,
            'amount' => 3000.00,
            'currency' => 'EUR'
        ]);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        //Term less than 10
        $I->sendPost('/client', [
            'clientId' => $clientId,
            'term' => 5,
            'amount' => 3000.00,
            'currency' => 'EUR'
        ]);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        //Term more than 30
        $I->sendPost('/client', [
            'clientId' => $clientId,
            'term' => 35,
            'amount' => 3000.00,
            'currency' => 'EUR'
        ]);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        //Amount less than 100.00
        $I->sendPost('/client', [
            'clientId' => $clientId,
            'term' => 30,
            'amount' => 92.50,
            'currency' => 'EUR'
        ]);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        //Amount more than 5000.00
        $I->sendPost('/client', [
            'clientId' => $clientId,
            'term' => 30,
            'amount' => 5500.50,
            'currency' => 'EUR'
        ]);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        //Wrong currency
        $I->sendPost('/client', [
            'clientId' => $clientId,
            'term' => 30,
            'amount' => 3000.00,
            'currency' => 'TTT'
        ]);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        //Wrong client id for get
        $I->sendGet('/application/200');
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        //Wrong client id for update
        $I->sendPut('/application/200');
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        //Wrong client id for delete
        $I->sendDelete('/application/200');
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);
    }
}